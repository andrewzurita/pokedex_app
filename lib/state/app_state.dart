import 'package:ffuf_flutter_architecture/ffuf_flutter_architecture.dart' hide Freezed;
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:pokedex/model/pokemon.dart';

part 'app_state.freezed.dart';
part 'app_state.g.dart';

@freezed
abstract class AppState with _$AppState {
  factory AppState({
    @JsonKey(name: 'pokemon') List<Pokemon> allPokemon,
  }) = _AppState;
  factory AppState.fromJson(Map<String, dynamic> json) => _$AppStateFromJson(json);
}

class AppStateSerializer extends StateSerializer<AppState> {
  @override
  AppState decode(Map<String, dynamic> data) => AppState.fromJson(data);

  @override
  Map<String, dynamic> encode(state) => state.toJson();
}
