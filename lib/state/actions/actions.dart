import 'package:ffuf_flutter_architecture/ffuf_flutter_architecture.dart';
import 'package:pokedex/apis/pokemon_list_request.dart';
import 'package:pokedex/model/pokemon.dart';
import 'package:pokedex/state/app_state.dart';

class ListAllPokemons extends ReduxAction<AppState> {
  @override
  Future<AppState> reduce() async {
    List<Pokemon> pokemons = await PokemonListRequest().listAllPokemons();

    return state.copyWith(allPokemon: pokemons);
  }
}
