import 'package:pokedex/model/pokemon.dart';
import './pokemon_csv.dart';

class PokemonListRequest {
  Future<List<Pokemon>> listAllPokemons() async {
    return PokemonCSV().getPokemons();
  }
}
