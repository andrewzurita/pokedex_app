import 'dart:convert';
import 'package:flutter/services.dart';
import 'package:pokedex/model/pokemon.dart';

class PokemonCSV {
  static Future<String> _loadAsset(String path) async {
    return await rootBundle.loadString(path);
  }

  Future<List<Pokemon>> getPokemons() async {
    List<Pokemon> allPokemon = [];

    await _loadAsset('assets/pokemon.csv').then((dynamic data) {
      LineSplitter.split(data).forEach((element) {
        List<String> items = element.split(',');
        allPokemon.add(Pokemon.fromCSV(items));
      });
    });
    return allPokemon;
  }
}
